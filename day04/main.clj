(require '[clojure.string :as str])

(defn count-valid-passphrases [lines check-anagrams]
  (if (< (count lines) 1) 0
  (+
    (let [line (map
            (if (true? check-anagrams) sort identity)
              (str/split (first lines) #" "))]
      (if (= (count (apply hash-set line)) (count line)) 1 0))
        (count-valid-passphrases (rest lines) check-anagrams))))

(let [input (line-seq (java.io.BufferedReader. *in*))]
  (println "Part one:" (count-valid-passphrases input false))
  (println "Part two:" (count-valid-passphrases input true)))
