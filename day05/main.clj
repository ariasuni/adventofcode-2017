(defn num-step-to-exit [input offset-mod]
  (def insts (transient input))
  (loop [index 0 num-step 1]
    (let [new-index (+ index (get insts index))]
    (assoc! insts index (offset-mod (get insts index)))
    (if (or (< new-index 0) (>= new-index (count insts)))
      num-step (recur new-index (inc num-step))))))

(defn inc-to-three-else-dec [val]
  (if (>= val 3) (dec val) (inc val)))

(let [input (vec (map read-string (line-seq (java.io.BufferedReader. *in*))))]
  (println "Part one:" (num-step-to-exit input inc))
  (println "Part two:" (num-step-to-exit input inc-to-three-else-dec)))
