(require '[clojure.string :as str])

(defn algo-one [line] (- (apply max line) (apply min line)))
(defn algo-two [line]
  (first (for [x (vec line) y (vec line)
               :let [result (/ x y)]
               :when (and (integer? result) (not (= 1 result)))] result)))

(defn checksum [algo lines]
  (let [line (map read-string (str/split (first lines) #"\t"))]
    (+ (algo line) (if (= (count lines) 1) 0 (checksum algo (rest lines))))))

(let [input (line-seq (java.io.BufferedReader. *in*))]
  (println "Part one:" (checksum algo-one input))
  (println "Part two:" (checksum algo-two input)))
