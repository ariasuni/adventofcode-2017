(defn sum-match
  ([string step] (sum-match string step 0))
  ([string step index]
    (let [current-char (get string index)
          compl-char (get string (mod (+ step index) (count string)))]
      (+
        (if (= current-char compl-char) (Character/digit current-char 10) 0)
        (if (= (inc index) (count string)) 0 (sum-match string step (inc index)))))))

(let [input (read-line)]
  (println "Part One:" (sum-match input 1))
  (println "Part Two:" (sum-match input (/ (count input) 2))))
