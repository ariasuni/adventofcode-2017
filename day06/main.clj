(require '[clojure.string :as str])

(defn get-new-banks [banks banks-len]
  (let [index (.indexOf banks (apply max banks))]
    (loop [amount (get banks index)
           banks (assoc banks index 0)
           cursor (mod (inc index) banks-len)]
      (if (= amount 0) banks
        (recur (dec amount)
               (update banks cursor inc)
               (mod (inc cursor) banks-len))))))

(defn num-steps-to-balance [banks]
  (def banks-len (count banks))
  (loop [banks banks
         configs (hash-map banks 0)
         num-step 1]
    (let [banks (get-new-banks banks banks-len)]
      (if (nil? (get configs banks))
        (recur banks (conj configs { banks num-step }) (inc num-step))
        [num-step (get configs banks)]))))

(let [input (vec (map read-string (str/split (read-line) #"\t")))
      [num-steps num-steps-prev] (num-steps-to-balance input)]
  (println "Part one:" num-steps)
  (println "Part two:" (- num-steps num-steps-prev)))
