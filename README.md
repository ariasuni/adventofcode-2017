# Advent of Code 2017 in Clojure

Each folder contains a solution as a single `main.clj` source file which computes both parts of the
day’s challenge for the `input` file.

Simply [install Clojure](https://clojure.org/guides/getting_started), then run
`clojure main.clj < input` (you can replace `input` with the file containing your input) in the
folder of the day you’re interested in. The executable then prints the two solutions in the
terminal.
