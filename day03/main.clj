(require '[clojure.core.reducers :as r])

(defn find-circle
  ([square] (if (= square 1) 0 (find-circle square 1 1)))
  ([square rayon last_sum]
    (let [sum (+ last_sum (* 4 (* 2 rayon)))]
      (if (>= sum square)
        (let [almost-side-len (* 2 rayon)
              rel-square (mod (+ (- sum square) rayon) almost-side-len)]
          (+ rayon (min rel-square (- almost-side-len rel-square))))
          (find-circle square (inc rayon) sum)))))

(defn compute-value [grid x y]
  (r/fold + (map #(get grid % 0) [
    [(dec x) (inc y)] [x (inc y)] [(inc x) (inc y)]
    [(dec x) y]                   [(inc x) y]
    [(dec x) (dec y)] [x (dec y)] [(inc x) (dec y)]])))

(defn find-first-larger
  ([input] (find-first-larger input 0 {[0 0] 1} 0 0))
  ([input rayon grid x y]
    (let [new-rayon (if (and (>= x 0) (= 0 (+ x y))) (inc rayon) rayon)
          [x y] (cond
            (= y (- rayon)) [(inc x) y] ; right
            (= x (- rayon)) [x (dec y)] ; down
            (= y rayon) [(dec x) y] ; left
            (= x rayon) [x (inc y)]) ; up))
          value (compute-value grid x y)]
      (if (> value input) value
        (find-first-larger input new-rayon (assoc grid [x y] value) x y)))))

(let [input (read-line)]
  (println "Part One:"(find-circle (read-string input)))
  (println "Part Two:" (find-first-larger (read-string input))))
